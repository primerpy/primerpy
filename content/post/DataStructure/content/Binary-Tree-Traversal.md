+++
title = "Preorder, Inorder and Postorder Traversal for a Binary Tree"
author = ["Isaac Zhou"]
lastmod = 2019-07-12T23:55:34-04:00
tags = ["data-structure", "binary-tree", "recursion"]
categories = ["Data Structure"]
draft = false
+++

## Problems {#problems}

There are three ways to traverse a binary tree: preorder, inorder and postorder.

On Leetcode, there are three problems


### 144. Binary Tree Preorder Traversal {#144-dot-binary-tree-preorder-traversal}

Given a binary tree, return the preorder traversal of its nodes' values.

```org
Input: [1,null,2,3]
   1
    \
     2
    /
   3

Output: [1,2,3]
```


### 94. Binary Tree Inorder Traversal {#94-dot-binary-tree-inorder-traversal}

Given a binary tree, return the inorder traversal of its nodes' values.

```org
Input: [1,null,2,3]
   1
    \
     2
    /
   3

Output: [1,3,2]
```


### 145. Binary Tree Postorder Traversal {#145-dot-binary-tree-postorder-traversal}

Given a binary tree, return the postorder traversal of its nodes' values.

```org
Input: [1,null,2,3]
   1
    \
     2
    /
   3

Output: [3,2,1]
```


## Though Process {#though-process}

They can all be done via recursion


## White Boards {#white-boards}


### Pre-Order {#pre-order}

{{< figure link="/img/lc/144_wb.JPG" >}}

### In-Order {#in-order}

{{< figure link="/img/lc/94_wb.JPG" >}}

### Post-Order {#post-order}

{{< figure link="/img/lc/145_wb.JPG" >}}

## Code {#code}


### Pre-Order {#pre-order}

```ipython
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    def _pre(self, root, res):

        if root:
            res.append(root.val)
            self._pre(root.left, res)
            self._pre(root.right, res)
        return res
        
        
    def preorderTraversal(self, root):
        
        res = []
        if root is None:
            return res

        return self._pre(root, res)
```


### In-Order {#in-order}

```ipython
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    def _inorder(self, root, res):
        if root:
            self._inorder(root.left, res)
            res.append(root.val)
            self._inorder(root.right, res)
        return res
    
    def inorderTraversal(self, root):

        res = []
        if root is None:
            return res
        
        return self._inorder(root, res)
```


### Post-Order {#post-order}

```ipython
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    def postorderTraversal(self, root):

        res = []
        if root is None:
            return res

        return self._post(root, res)

    def _post(self, root, res):
        if root:
            self._post(root.left, res)
            self._post(root.right, res)
            res.append(root.val)

        return res
```
