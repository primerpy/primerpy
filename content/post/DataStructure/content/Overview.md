+++
title = "Data Structure and Algorithms Overview"
author = ["Isaac Zhou"]
lastmod = 2019-07-27T17:10:52-04:00
tags = ["data-structure", "algorithms"]
categories = ["Data Structure"]
draft = false
+++

Below is an overview of data structure and algorithms. I believe they will cover at least 85% of the use case.

{{< figure link="/img/ds/Data Structure and Algorithms.png" >}}

There will be more updates/contents added in the future.
