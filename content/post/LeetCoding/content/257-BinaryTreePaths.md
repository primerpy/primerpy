+++
title = "257. Binary Tree Paths"
author = ["Isaac Zhou"]
lastmod = 2019-07-18T21:31:19-04:00
tags = ["lc-recursion"]
categories = ["Leetcoding"]
draft = false
+++

## Problem {#problem}

Given a binary tree, return all root-to-leaf paths.

Note: A leaf is a node with no children.

Example:

```org
Input:
   1
 /   \
2     3
 \
  5
Output: ["1->2->5", "1->3"]

Explanation: All root-to-leaf paths are: 1->2->5, 1->3
```


## Thought Process & White Board {#thought-process-and-white-board}

{{< figure link="/img/lc/257_tp.JPG" >}}
{{< figure link="/img/lc/257_wb.JPG" >}}

## Code {#code}

```ipython
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    def binaryTreePaths(self, root):
        res = []

        if root is None:
            return res

        if root.left is None and root.right is None:
            res.append(str(root.val))
            return res

        if root.left:
            left_res = self.binaryTreePaths(root.left)

            for item in left_res:
                res.append(str(root.val) + "->" + str(item))

        if root.right:
            right_res = self.binaryTreePaths(root.right)

            for item in right_res:
                res.append(str(root.val) + "->" + str(item))

        return res
```

{{< figure link="/img/lc/257_output.png" >}}
