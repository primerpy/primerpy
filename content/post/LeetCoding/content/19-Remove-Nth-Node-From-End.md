+++
title = "19. Remove Nth Node From End of List"
author = ["Isaac Zhou"]
lastmod = 2019-07-17T23:47:43-04:00
tags = ["lc-linked-lists"]
categories = ["Leetcoding"]
draft = false
+++

## Problem {#problem}

Given a linked list, remove the n-th node from the end of list and return its head.

Example:

```org
Given linked list: 1->2->3->4->5, and n = 2.

After removing the second node from the end, the linked list becomes 1->2->3->5.
```

## Thought Process & White Board {#thought-process-and-white-board}

{{< figure link="/img/lc/19-tp-wb_1.JPG" >}}
{{< figure link="/img/lc/19-tp-wb_2.JPG" >}}
{{< figure link="/img/lc/19-tp-wb_3.jpg" >}}


## Code {#code}

```ipython
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def removeFromEnd(self, head, n):
        assert(n)
        dummyHead = ListNode(None)
        dummyHead.next = head

        #initialize
        p, q = dummyHead, dummyHead

        for i in range(n+1):
            assert(q)
            q = q.next

        # now p, q distance is n-1
        while q:
            p = p.next
            q = q.next

        delNode = p.next

        p.next = delNode.next

        return delNode
```

```org

```
