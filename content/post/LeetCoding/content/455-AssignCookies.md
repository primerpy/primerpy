+++
title = "455-AssignCookies"
author = ["Isaac Zhou"]
lastmod = 2019-07-09T22:21:02-04:00
tags = ["lc-greedy"]
categories = ["Leetcoding"]
draft = false
+++

## Problem {#problem}

Assume you are an awesome parent and want to give your children some cookies. But, you should give each child at most one cookie. Each child i has a greed factor gi, which is the minimum size of a cookie that the child will be content with; and each cookie j has a size sj. If sj >= gi, we can assign the cookie j to the child i, and the child i will be content. Your goal is to maximize the number of your content children and output the maximum number.

Note:
You may assume the greed factor is always positive.
You cannot assign more than one cookie to one child.

Example 1:

```org
Input: [1,2,3], [1,1]

Output: 1

Explanation: You have 3 children and 2 cookies. The greed factors of 3 children are 1, 2, 3.
And even though you have 2 cookies, since their size is both 1, you could only make the child whose greed factor is 1 content.
You need to output 1.
```

Example 2:

```org
Input: [1,2], [1,2,3]

Output: 2

Explanation: You have 2 children and 3 cookies. The greed factors of 2 children are 1, 2.
You have 3 cookies and their sizes are big enough to gratify all of the children,
You need to output 2.
```


## Thought Process {#thought-process}

-   Sort the size of cookies and size of the greed.
-   Check if the largest cookie will satisfy the greediest kid, if yes, check the next largest cookie vs the next greediest kid
-   if no, check if the largest cookie will satisfy the next greediest kid
-   loop till the end

{{< figure link="/img/lc/455_tp.JPG" >}}

## White Board {#white-board}

Below is the white board:

{{< figure link="/img/lc/455_wb.JPG" >}}

## Code {#code}

```ipython
class Solution:
    def findContentChildren(self, g, s):
        g = sorted(g, reverse=True) # O(nlogn)
        s = sorted(s, reverse=True) # O(nlogn)

        res = 0

        g_i, s_i = 0, 0

        while g_i < len(g) and s_i < len(s):
            if s[s_i] >= g[g_i]:
                res += 1
                g_i += 1
                s_i += 1
            else:
                g_i += 1 # check if it will satisfy the next greedies kid

        return res

def test():
    g = [1,2,3]
    s = [1,2,1]
    fcc = Solution().findContentChildren(g,s)
    print(fcc)

test()
```

```org
2
```


## Complexity {#complexity}

-   Time complexity is O(nlogn), as I need to sort both lists first.


## Optimization {#optimization}

-   How to optimize it?
