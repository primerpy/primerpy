+++
title = "220. Contains Duplicate III"
author = ["Isaac Zhou"]
lastmod = 2019-07-19T21:50:13-04:00
tags = ["lc-binary-tree"]
categories = ["Leetcoding"]
draft = false
+++

## Problem {#problem}

Given an array of integers, find out whether there are two distinct indices i and j in the array such that the absolute difference between nums[i] and nums[j] is at most t and the absolute difference between i and j is at most k.

Example 1:

```org
Input: nums = [1,2,3,1], k = 3, t = 0
Output: true
```

Example 2:

```org
Input: nums = [1,0,1,1], k = 1, t = 2
Output: true
```

Example 3:

```org
Input: nums = [1,5,9,1,5,9], k = 2, t = 3
Output: false
```


## Thought Process and White Board {#thought-process-and-white-board}
{{< figure link="/img/lc/220_tp.JPG" >}}

{{< figure link="/img/lc/220_wb.JPG" >}}

## Code {#code}

```ipython
class Solution:
    def containsNearbyAlmostDuplicate(self, nums, k, t):
        if k < 1 or t < 0:
            return False
        dic = collections.OrderedDict()
        for n in nums:
            key = n if not t else n // t
            for m in (dic.get(key - 1), dic.get(key), dic.get(key + 1)):
                if m is not None and abs(n - m) <= t:
                    return True
            if len(dic) == k:
                dic.popitem(False)
            dic[key] = n
        return False
```

