+++
title = "347. Top K Frequent Elements"
author = ["Isaac Zhou"]
lastmod = 2019-07-21T14:34:51-04:00
tags = ["lc-queue"]
categories = ["Leetcoding"]
draft = false
+++

## Problem {#problem}

Given a non-empty array of integers, return the k most frequent elements.

Example 1:

```org
Input: nums = [1,1,1,2,2,3], k = 2
Output: [1,2]
```

Example 2:

```org
Input: nums = [1], k = 1
Output: [1]
```

Note:

-   You may assume k is always valid, 1 ≤ k ≤ number of unique elements.
-   Your algorithm's time complexity must be better than O(n log n), where n is the array's size.


## Thought Process and White Board {#thought-process-and-white-board}

-   Use a hash-table to count the frequency of each number
-   Sort the hash-table
-   Return the top k elements by value

{{< figure link="/img/lc/347_tp.JPG" >}}
{{< figure link="/img/lc/347_wb.JPG" >}}

## Code {#code}

```ipython
class Solution:
    def _create_ht(self, nums):
        ht = {}
        for num in nums:
            if num in ht:
                ht[num] += 1
            else:
                ht[num] = 1
        return ht

    def _ord_dict_val(self, my_dict, k):
        ord_tuples = sorted([(v,k) for (k,v) in my_dict.items()], reverse=True)[:k]
        return {k:v for (v,k) in ord_tuples}


    def topKFrequent(self, nums, k):

        ht = self._create_ht(nums)
        if k < 0 or k > len(ht):
            return -1
        ord_dict = self._ord_dict_val(ht, k)

        return ord_dict.keys()
```

{{< figure link="/img/lc/347_output.png" >}}
