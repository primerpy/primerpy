+++
title = "226. Invert Binary Tree"
author = ["Isaac Zhou"]
lastmod = 2019-07-08T20:55:54-04:00
tags = ["lc-binary-tree"]
categories = ["Leetcoding"]
draft = false
+++

## Problem {#problem}

Invert a binary tree.

```org
     4
   /   \
  2     7
 / \   / \
1   3 6   9
```

Output:

```org
     4
   /   \
  7     2
 / \   / \
9   6 3   1
```


## Thought Process {#thought-process}

Can solve this question recursively.

-   if the root is None, return None

-   else, revert root's left tree recursively, revert root's right tree recursively

-   swap left, right tree


## White Board {#white-board}

Below is the white board:

{{< figure link="/img/lc/226_wb.JPG" >}}

## Code {#code}

```ipython
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    def invertTree(self, root: TreeNode) -> TreeNode:
        if root is None:
            return None

        self.invertTree(root.left)
        self.invertTree(root.right)
        self.left, self.right = self.right, self.left

        return root
```

{{< figure link="/img/lc/226_output.png" >}}


## Complexity {#complexity}

-   Time complexity is O(n)


## Optimization {#optimization}

Maybe have an iterative solution...

