+++
title = "104. Maximum Depth of Binary Tree"
author = ["Isaac Zhou"]
lastmod = 2019-07-08T20:41:28-04:00
tags = ["lc-binary-tree"]
categories = ["Leetcoding"]
draft = false
+++

## Problem {#problem}

Given a binary tree, find its maximum depth.

The maximum depth is the number of nodes along the longest path from the root node down to the farthest leaf node.

Note: A leaf is a node with no children.

Example:

Given binary tree [3,9,20,null,null,15,7],

```org
    3
   / \
  9  20
    /  \
   15   7
```

return its depth = 3.


## Thought Process {#thought-process}

Can solve this question recursively.

-   find the max d for the left tree recursively
-   find the max d for the right tree recursively
-   return the larger d

{{< figure link="/img/lc/104_tp.JPG" >}}

## White Board {#white-board}

Below is the white board:

{{< figure link="/img/lc/104_wb.JPG" >}}


## Code {#code}

```ipython
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    def maxDepth(self, root: TreeNode) -> int:
        if root is None:
            return 0
        left_d = self.maxDepth(root.left)
        right_d = self.maxDepth(root.right)

        return max(left_d, right_d) + 1
```

```org

```


## Complexity {#complexity}

-   Time complexity is O(n)


## Optimization {#optimization}

Maybe have an iterative solution...

```ipython

```

```org

```
