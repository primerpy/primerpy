+++
title = "112. Path Sum"
author = ["Isaac Zhou"]
lastmod = 2019-07-20T20:43:22-04:00
tags = ["lc-recursion"]
categories = ["Leetcoding"]
draft = false
+++

## Problem {#problem}

Given a binary tree and a sum, determine if the tree has a root-to-leaf path such that adding up all the values along the path equals the given sum.

Note: A leaf is a node with no children.

Example:

Given the below binary tree and sum = 22,

```org
      5
     / \
    4   8
   /   / \
  11  13  4
 /  \      \
7    2      1
```

return true, as there exist a root-to-leaf path 5->4->11->2 which sum is 22.


## Thought Process & White Board {#thought-process-and-white-board}

{{< figure link="/img/lc/112-tp_1.JPG" >}}
{{< figure link="/img/lc/112-tp_2.JPG" >}}
{{< figure link="/img/lc/112-tp_3.JPG" >}}
{{< figure link="/img/lc/112-tp_4.JPG" >}}
{{< figure link="/img/lc/112-wb.JPG" >}}

## Code {#code}

```ipython
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    def hasPathSum(self, root, num):

        if root is None:
            return False

        if root.left is None and root.right is None:
            return root.val == num

        if self.hasPathSum(root.left, num - root.val):
            return True

        if self.hasPathSum(root.right, num - root.val):
            return True
```
{{< figure link="/img/lc/112-output.png" >}}
