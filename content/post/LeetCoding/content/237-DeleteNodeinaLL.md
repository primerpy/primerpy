+++
title = "237. Delete Node in a Linked List"
author = ["Isaac Zhou"]
lastmod = 2019-07-11T12:07:30-04:00
tags = ["lc-linked-lists"]
categories = ["Leetcoding"]
draft = false
+++

## Problem {#problem}

Write a function to delete a node (except the tail) in a singly linked list, given only access to that node.

Given linked list -- head = [4,5,1,9], which looks like following:

```org
4->5->1->9
```

Example 1:

```org
Input: head = [4,5,1,9], node = 5
Output: [4,1,9]
Explanation: You are given the second node with value 5, the linked list should become 4 -> 1 -> 9 after calling your function.
```

Example 2:

```org
Input: head = [4,5,1,9], node = 1
Output: [4,5,9]
Explanation: You are given the third node with value 1, the linked list should become 4 -> 5 -> 9 after calling your function.
```

Note:

-   The linked list will have at least two elements.
-   All of the nodes' values will be unique.
-   The given node will not be the tail and it will always be a valid node of the linked list.
-   Do not return anything from your function.


## Thought Process {#thought-process}

-   The problem only provided the node, not head node, so we can't traverse the linked list to find the node to delete.
-   An easy way to solve is to replace the node's current value with the next node value

{{< figure link="/img/lc/237_tp.JPG" >}}

## White Board {#white-board}

Below is the white board:

{{< figure link="/img/lc/237_wb.JPG" >}}

## Code {#code}

```ipython
class LN:
    def __init__(self, val):
        self.val = val
        self.next = None

class Sol:
    def deleteNode(self, node):

        if node is not None:
            delnode = node.next
            node.val = delnode.val

            #find the next node to replace/delete
            if delnode.next is not None:
                node.next = delnode.next
            else:
                node.next = None

        node = None
```

```org

```


## Complexity {#complexity}

-   Time complexity is O(1), space complexity is O(1) as well


## Optimization {#optimization}


### <span class="org-todo todo TODO">TODO</span> How to improve? {#how-to-improve}
