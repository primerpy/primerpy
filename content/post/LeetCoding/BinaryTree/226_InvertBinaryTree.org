#+TITLE: 226. Invert Binary Tree
#+HUGO_BASE_DIR: ../
#+HUGO_SECTION: ./
#+HUGO_WEIGHT: auto
#+HUGO_TAGS: "lc-binary-tree"
#+HUGO_CATEGORIES: "Leetcoding"
#+HUGO_AUTO_SET_LASTMOD: t

* Problem

Invert a binary tree.

#+begin_src org
     4
   /   \
  2     7
 / \   / \
1   3 6   9
#+end_src

Output:

#+begin_src org
     4
   /   \
  7     2
 / \   / \
9   6 3   1
#+end_src


* Thought Process
Can solve this question recursively.
- if the root is None, return None

- else, revert root's left tree recursively, revert root's right tree recursively

- swap left, right tree


* White Board

Below is the white board:

* Code

#+BEGIN_SRC ipython :session :exports both :results output org
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    def invertTree(self, root: TreeNode) -> TreeNode:
        if root is None:
            return None

        self.invertTree(root.left)
        self.invertTree(root.right)
        self.left, self.right = self.right, self.left

        return root
#+END_SRC


* Complexity

- Time complexity is O(n)
  
* Optimization

Maybe have an iterative solution...

#+BEGIN_SRC ipython :session :exports both :results output org
#+END_SRC
