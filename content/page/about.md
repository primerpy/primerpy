---
title: About PrimerPy
subtitle: What is PrimerPy and Why this site is created
comments: false
---

# PrimerPy

Primerpy provides a primer on python and related data science topics. It covers all the prerequisites for Deep Learning, Machine Learning, and Artificial Intelligence. It is designated for tech stacks and programming languages used in data science.

So what are those?

### [Python](http://www.primerpy.com/tags/python/) 
> Python is a clear winner in data science. We cover not only the native Python features, such as loops, functions and objects, but also important libraries such as Numpy, Pandas, Matplotlib, Bokeh, scikit-learn, statsmodels and sqlalchemy etc.

### [Javascript](http://www.primerpy.com/tags/Javascript/)
> As a Data Scientist, one of the biggest challenges I face is to convert my research and work to a product, particularly for the visualization part. Although we have libraries such as decades old matplotlib (...uh), promising-yet-not-there Bokeh, plotly (can't say I'm a big fan) etc, none of these can compete with D3 and its derivatives. Also with latest technologies and updates such as React and ES2015, it's exciting to revigorate Javascript now.

### [R](http://www.primerpy.com/tags/R/)
> Compared to Python, I found R a little bit more popular among academicians. Still, it's a powerful and great language to learn, particularly some libraries are excellent, e.g. ggplot2, tidyr... Plus, with R Markdown, R is able to create some really nice reporting and websites.

### [SQL](http://www.primerpy.com/tags/SQL/)
> Dah! Why are there so many SQL-like scripting languages to stream data from NoSQL? There's a reason. Just learn it.
