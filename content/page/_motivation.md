---
title: About PrimerPy
subtitle: What is PrimerPy and Why this site is created
comments: false
draft: true
---

The world is fundamentally transition and in this fast changing environment, the most precious commodity is not time, nor money (fiat or crypto) but focus. 

Unlike the unproductive and finger-pointing organizations from the last age, in this new age, with laser-focus, tenancity, patience and resilience, any individual can achieve amazing accomplishments. Need proof?

* Ever heard of [Vitalik Buterin](https://en.wikipedia.org/wiki/Vitalik_Buterin), the creator of Ethereum

* How about [George Hotz](https://en.wikipedia.org/wiki/George_Hotz), who basically built a self-driving car on his own 

The purpose is not to negating necessary collaborative work from proactive communities, but to promote individual focus. 

The focus of this website is on the data science and the technologies involved. We will cover the following aspects:

* Data Cleaning and Preparation

* Data Analysis

* Modeling

* Visualization

* Communication and Reporting

* MVP (Minimal Viable Product) if applicable
